<?php

class VistaMensaje{

	// Atributos
	private $_mensaje;

	// Getters y Setters

	/// Construccion
	public function __construct(string $pMensaje){
		$this->_mensaje = $pMensaje;
	}

	// Métodos de interface
	public function imprimir(){	
		echo($this->_mensaje);
	}

}