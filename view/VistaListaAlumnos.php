<?php

class VistaListaAlumnos{
	
	// Atributos
	private $_profesor;
	
	// Getters y Setters
	
	/// Construccion
	public function __construct(Profesor $pProfesor){
		$this->_profesor = $pProfesor;
	}
	
	// Métodos de interface
	public function imprimir(){
		
		// Imprime cabecera de la tabla
		echo("<table style='width:30%''>");
		echo("<tr><th align='left'>Nombre</th><th align='left'>Apellidos</th></tr>");
		foreach ($this->_profesor->dameTusAlummos() as $unAlumno) {
			$unNombre = $unAlumno->_nombre;
			$unApellido = $unAlumno->_apellido1;
			echo("<tr><td>$unNombre</th><td>$unApellido</th></tr>");
		}
		echo("</table>");
		
		echo("<a href=index.php>Volver</a>");
	}

}