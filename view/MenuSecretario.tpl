<div style="">
	
	<div id="panelMatricula">
		<button id="botonMatriculaAlumno" onclick="matriculaAlumno_onClick()">Matrícula alumno</button>
	</div>
	<button id="logout" onclick="logout_onClick()">Logout</button>

</div>

<script>

	function matriculaAlumno_onClick(){
		my_form=document.createElement('FORM');
		my_form.name='myForm';
		my_form.method='GET';
		my_form.action='index.php';

		my_tb=document.createElement('INPUT');
		my_tb.type='TEXT';
		my_tb.name='dni';
		my_tb.placeholder="DNI";
		my_form.appendChild(my_tb);
		
		my_tb=document.createElement('INPUT');
		my_tb.type='hidden';
		my_tb.name='accion';
		my_tb.value="1";
		my_form.appendChild(my_tb);
		
		my_tb=document.createElement('INPUT');
		my_tb.type='submit';
		my_tb.value='Matrícula alumno';
		my_form.appendChild(my_tb);

		document.getElementById("panelMatricula").appendChild(my_form);
		document.getElementById("botonMatriculaAlumno").remove();
	}
	
	function logout_onClick(){
		post("index.php", {accion: "5"}, "get")
	}
	
	
	 /**
 * sends a request to the specified url from a form. this will change the window location.
 * @param {string} path the path to send the post request to
 * @param {object} params the paramiters to add to the url
 * @param {string} [method=post] the method to use on the form
 */

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

</script>