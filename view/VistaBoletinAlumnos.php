<?php

class VistaBoletinAlumnos{
	
	// Atributos
	private $_profesor;
	
	// Getters y Setters
	
	/// Construccion
	public function __construct(Profesor $pProfesor){
		$this->_profesor = $pProfesor;
	}
	
	// Métodos de interface
	public function imprimir(){
		
		$listaAlumnos = $this->_profesor->dameTusAlummos();
		$primerAlumno = $listaAlumnos[0];

		//Comienzo la impresión de la tabla;
		echo("<table style='width:30%''>");
	
		// Cojo los exámenes del primer alumno de referencia para pintar la cabecera del listado
		$this->imprimirCabeceraBoletinNotas($primerAlumno->dameTusExamenes());

		// Imprime todas las notas de los alumnos
		$this->imprimirCuerpoBoletinDeNotas($listaAlumnos);

		//Termino la impresión de la tabla
		echo('</table>');
		
		echo("<a href=index.php>Volver</a>");
	}
	
	// Métodos privados
	private function imprimirCabeceraBoletinNotas(array $pListaExamenes){
		$numExamenTeorico = 1;
		$numExamenPractico = 1;
		
		//Comienzo la impresión de la cabecera
		echo("<tr>");
		echo("<th align='left'>Nombre</th>");
		echo("<th align='left'>Apellidos</th>");

		//Listado de los exámenes resueltos
		foreach($pListaExamenes as $i => $unExamen){  
			if($unExamen instanceof ExamenTeorico){
				echo("<th align='center'>T$i</th>");
				$numExamenTeorico++;
			}
			else if($unExamen instanceof ExamenPractico){
				echo("<th align='center'>P$i</th>");
				$numExamenPractico++;
			}
		}
		
		//Termino la impresión de la cabecera
		echo("<th align='left'>Media</th>");
		echo("</tr>");
	  }
	  
	   private function imprimirCuerpoBoletinDeNotas(array $pListaAlumnos){
	 	  usort($pListaAlumnos, function ($a, $b){
			  return $a->dameMediaDeTusExamenes() < $b->dameMediaDeTusExamenes();
		  });
		  
		  foreach ($pListaAlumnos as $key => $unAlumno) {
			  echo("<tr>");
			  echo("<td>$unAlumno->_nombre</td><td>$unAlumno->_apellido1</td>");
			  foreach ($unAlumno->dameTusExamenes() as $unExamen) {
				printf("<td>%.2f</td>",$unExamen->_nota);
			  }
			  printf("<td>%.2f</td>", $unAlumno->dameMediaDeTusExamenes());
			  echo("</tr>");
		  }
	   }

}