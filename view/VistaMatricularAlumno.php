<?php

class VistaMatricularAlumno{

	// Atributos
	private $_alumno;
	private $_secretario;
	
	// Getters y Setters
	
	/// Construccion
	public function __construct(Alumno $pAlumno, Secretario $pSecretario){
		$this->_alumno = $pAlumno;
		$this->_secretario = $pSecretario;
	}
	
	// Métodos de interface
	public function imprimir(){	
		echo "<br>Nombre: ".$this->_alumno->_nombre;
		echo "<br>Apellidos: ".$this->_alumno->_apellido1." ".$this->_alumno->_apellido2;
		
		$matriculas = $this->_alumno->getMatriculas();
		
		if(count($matriculas)>0){
			echo "<br><br>Tiene Matrículas en los siguientes cursos: ";
			echo "<ul>";
			foreach($this->_alumno->getMatriculas() as $unaMateria){
				echo("<LI>$unaMateria->_nombre</LI>");
			}
			echo "</ul>";
		}
		
		$posiblesMatriculas = array();
		foreach($this->_secretario->getMateriasMatriculables() as $unaMateria){
			if(!$this->_alumno->estasMatriculadoEn($unaMateria)){
				array_push($posiblesMatriculas, $unaMateria);
			}
		}
		
		if(count($posiblesMatriculas)>0){
			echo("<br><br>Puede Matricularse en:");
			$this->imprimeFormMatricula($posiblesMatriculas);
		}
		
		echo("<a href=index.php>Volver</a>");
	}
		
	private function imprimeFormMatricula($pMaterias){
		echo("<form action='index.php'>");
		//TODO:
		https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_input_type_checkbox
		echo("</form>");
	}	

}