# README

## Para qué es este repo?

Para realizar el Examen práctico de la 2ª EV en DWCS

## Concreciones sobre especificaciones


1. Para este caso (el aplicativo web), va a disponer de un sencillo formulario de login (usuario/password). Si las credenciales son correctas se tendrá acceso a la interfaz. Asegúrate de que las credenciales sean estas: usuario: `prof`; usuario: `alu`; usuario: `secr` y en todos los casos el password: `123`.

2. Tu aplicativo debe ser accesible a través del recurso `index.php` existente en la raiz del repo.

3. El formato para concretar las especificaciones se ecuentra en la hoja del examen.

4. Cíñete a los lugares que se especifican en la hoja del examen para implementar tus respuestas.


## Pre-requisitos

Haber realizado total o parcialmente la práctica planteada.

## Lo que se pretende comprobar en este ejercicio

* Que tengas la capacidad de leer y comprender el código con el que se parte.
* Que tengas la capacidad de corregir el funcionamiento donde se te ha indicando
* Que tengas la capacidad de implementar una nueva funcionalidad basándote en un diseño pre establecido


## Antes de ponerte a trabajar...

### Haz un fork del repositorio original

Haz un fork del repositorio original y configúralo de forma privada (el examen es individual ;)


### Clona el repositorio

```
git clone <url de tu fork>
```

### Ejecuta el programa

Despliega el directorio en un virtualhost de Apache para que este interprete los scripts.

### Crea tu rama (personal) de trabajo

Crea tu propia rama de trabajo! Crea una nueva rama a partir de master que se llame como el nombre de tu usuario en el curso. Te recuerdo cómo:

```
git checkout -b <usuario>
```

La evolución de tu solución final (si no estás trabajando en equipo) deberá estar apuntada por esta rama. Puedes utilizar todas las ramas que quieras, pero **no trabajes en la master** y asegúrate, si tienes otras ramas que forman parte de tu solución, de combinarlas con tu rama con el nombre de tu usuario.

## Cuándo termines tu trabajo... o eso crees...

### Etiqueta tu versión

Pon la siguiente etiqueta:
```
examen-tuUsuario
```

### Snapshot actual del enunciado:

```Shell
.
├── README.md
├── controller
│   ├── MenuProfesor.php
│   └── MenuSecretario.php
├── index.php
├── login.php
├── model
│   ├── Alumno.php
│   ├── Examen.php
│   ├── ExamenPractico.php
│   ├── ExamenTeorico.php
│   ├── Materia.php
│   ├── Persona.php
│   ├── Profesor.php
│   └── Secretario.php
└── view
    ├── MenuProfesor.tpl
    ├── MenuSecretario.tpl
    ├── VistaBoletinAlumnos.php
    ├── VistaListaAlumnos.php
    ├── VistaMatricularAlumno.php
    ├── VistaMensaje.php
    ├── VistaMenuProfesor.php
    └── VistaMenuSecretario.php
```

## Contribution guidelines

* Writing tests
* Code review
* Other guidelines

## Who do I talk to?

* Repo owner or admin
* Other community or team contact