<?php

require_once("controller/MenuProfesor.php");
require_once("controller/MenuSecretario.php");
require_once("model/Alumno.php");
require_once("model/Profesor.php");
require_once("model/Secretario.php");
require_once("model/Materia.php");


// Iniciando o retomando sesion
session_start();

if(!isset($_SESSION["login"]) or !$_SESSION["login"] === true){
	header('Location: login.php');
	die;
}

if(!isset($_SESSION["menu"])){
	
	// Precarga de datos de prueba
	$alumno1 = new Alumno("11", "Mateo", "Perez", "Fernández");
	$alumno2 = new Alumno("22", "Julia", "Perez", "Fernández");
	$alumnos = [$alumno1, $alumno2];
	$profesor1 = new Profesor();
	$secretario1 = new Secretario();
	$secretario1->addMateriaMatriculable(new Materia("PO","Programación"));
	$secretario1->addMateriaMatriculable(new Materia("SI","Sistemas"));
	
	// Asignaciones
	$profesor1->asignaAlumnos($alumnos);
	$secretario1->asignaAlumnos($alumnos);
	
	// Creo un menú para el profesor y lo agrego a su session
	if($_SESSION['login_user'] == "prof"){
		$menu = new MenuProfesor($profesor1);
	}
	else if($_SESSION['login_user'] == "secr") {
		$menu = new MenuSecretario($secretario1);
	}
	else {
		die;
	}
	
	// Asigno el menú a la session
	$_SESSION["menu"] = $menu;
}
else {
	$menu = $_SESSION["menu"];
}

$vistas = $menu->procesaAccion($_GET);

?>


<html>
<body>
	<?php 
		if(isset($vistas) and is_iterable($vistas)){
			foreach ($vistas as $vista) $vista->imprimir();
		}
	?>
</body>
</html>