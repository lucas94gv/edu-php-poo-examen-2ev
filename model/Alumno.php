<?php 

class Alumno {
	
	//De clase
	private static $autoIncremental = 0;
	
	// Atributos
	private $_idAlumno;
	private $_dni;
	private $_nombre;
	private $_apellido1;
	private $_apellido2;
	
	private $_misExamenes = array();
	private $_misMatriculas = array();
	
	// Getters y Setters
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}
	
	// Construccion
	public function __construct(string $pDni, string $pNombre, string $pApellido1, string $pApellido2) {
		//this->$_idAlumno = ++self::$autoIncremental;
		$this->_dni = $pDni;
		$this->_nombre = $pNombre;
		$this->_apellido1 = $pApellido1;
		$this->_apellido2 = $pApellido2;
	}
	
	// Métodos de interface
	public function entregaExamen(Examen $pExamen) {
		array_push($this->_misExamenes,$pExamen);
	}

	// Métodos de interface
	public function dameTusExamenes() {
		return $this->_misExamenes;
	}

	public function dameMediaDeTusExamenes() {
		$acumulado = 0;
		$numExamenes = 0;
		foreach ($this->dameTusExamenes() as $unExamen) {
			$acumulado += $unExamen->_nota;
			$numExamenes++;
		}
		if ($numExamenes > 0)
			return $acumulado / $numExamenes;
		else
			return (float) 0;
	}
	
	public function matricularEnMateria(Materia $pMateria) {
		array_push($this->_misMatriculas, $pMateria);
	}
	
	public function getMatriculas(){
		//TODO:
	}
	
	public function estasMatriculadoEn(Materia $pMateria){
		//TODO:
		return false;
	}
	
}