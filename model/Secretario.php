<?php

require_once("Alumno.php");

class Secretario {
	//Atributos
	private $_alumnos = array();
	private $_materias = array();
	
	// Getters y Setters
	private function getAlumnos() {
		return $this->_alumnos;
	}
	
	// Construcción
	
	
	// Métodos de interface
	public function getAlumnoFromDni(string $pDni):Alumno {
		$a = $this->getAlumnos();
		return $a[$pDni];
	}

	public function createAlumno(String $pDni, String $pNombre, String $pApellido1, String $pApellido2) {
		$nuevoAlumno = new Alumno($pDni, $pNombre, $pApellido1, $pApellido2);
		$_alumnos[$pDni] = $nuevoAlumno;
		return $nuevoAlumno;
	}

	public function addMateriaMatriculable(Materia $pMateria) {
		//TODO:
	}

	public function getMateriasMatriculables() {
		//TODO:
	}
	
	public function getMateriaPorCodigo($pCod){
		//TODO:
	}

	public function asignaAlumnos(array $pAlumnos) {
		//TODO:
	}

	// Métodos privados
}