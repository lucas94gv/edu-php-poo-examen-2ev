<?php
require_once("ExamenTeorico.php");
require_once("ExamenPractico.php");

class Profesor {
	//Atributos
	private $_misAlumnos = array();

	//Getters y Setters

	//Construcción
	
	//Métodos de interface
	public function dameTusAlummos() {
		return $this->_misAlumnos;
	}

	public function asignaAlumnos(Array $pAlumnos) {
		foreach ($pAlumnos as $unAlumno) {
			array_push($this->_misAlumnos, $unAlumno);
		}
	}

	public function simulaExamenTeorico() {
		foreach ($this->_misAlumnos as $unAlumno) {
			$this->simulaExamenParaAlumno($unAlumno, new ExamenTeorico(), 0, 10);
		}
		syslog(LOG_DEBUG, "Las notas ya han sido asignadas a los alumnmos");
	}

	public function simulaExamenPractico() {
		foreach ($this->_misAlumnos as $unAlumno) {
			$this->simulaExamenParaAlumno($unAlumno, new ExamenPractico(), 0, 10);
		}
		syslog(LOG_DEBUG, "Las notas ya han sido asignadas a los alumnmos");
	}

	//Métodos privados
	private function simulaExamenParaAlumno(Alumno $pAlumno, Examen $pExamen, int $pNotaMin, int $pNotaMax) {
		$valorEntero = rand($pNotaMin, $pNotaMax);
		$pAlumno->entregaExamen($pExamen->evaluaExamen($valorEntero));
	}
}