<?php 

class Examen { 

	// Atributos
	private  $_nota;
	
	// Getters y Setters
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		
		return $this;
	}
	
	// Construccion
	
	
	// Métodos de interface
	public function evaluaExamen(float $pNota) {
		$this->_nota = $pNota;
		return $this;
	}
}