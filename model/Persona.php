<?php

interface Persona {
	public function get_nombre() : string;
	public function get_apellido1(): string;
	public function get_apellido2(): string;
}