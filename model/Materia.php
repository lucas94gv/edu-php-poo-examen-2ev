<?php

class Materia {
	//Atributos
	private $_nombre;
	private $_codigo;

	//Getters y Setters
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		return $this;
	}

	// Construccion
	public function __construct(string $pCodigo, string $pNombre) {
		$this->_nombre = $pNombre;
		$this->_codigo = $pCodigo;
	}
	
	//Métodos de interface

	//Métodos privados
}