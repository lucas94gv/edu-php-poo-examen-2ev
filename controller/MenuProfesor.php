<?php 

require_once(__DIR__ ."/../view/VistaMenuProfesor.php");
require_once(__DIR__ ."/../view/VistaMensaje.php");
require_once(__DIR__ ."/../view/VistaListaAlumnos.php");
require_once(__DIR__ ."/../view/VistaBoletinAlumnos.php");

class MenuProfesor{
	
	// Atributos
	private $_profesor;
	
	// Getters y Setters
	
	
	// Construccion
	public function __construct(Profesor $pProfesor){
		$this->_profesor = $pProfesor;
	}
	
	// Métodos de interface
	public function procesaAccion(array $request){
		$accion = !isset($request["accion"]) ? 0 : $request["accion"];
		if($accion == 0){
			return [$this->getMenu(), new VistaMensaje("Selecciona una opción")];
		}
		else if($this->esAccionValida($accion)){
			switch($accion){
				case 1:
					return [new VistaListaAlumnos($this->_profesor)];
				case 2:
					//TODO:
					break;
				case 3:
					$accion = !isset($request["examen"]) ? 0 : $request["examen"];
					if($accion == "teorico")
						$this->_profesor->simulaExamenTeorico();
					else if($accion == "practico")
						$this->_profesor->simulaExamenPractico();
					else die("No se ha especificado el tipo de examen");
					
					return [new VistaMensaje("Las notas ya han sido asignadas a los alumnos"), 
							new VistaBoletinAlumnos($this->_profesor)];
				case 4:
					return [new VistaBoletinAlumnos($this->_profesor)];
				case 5:
					session_destroy();
					// https://stackoverflow.com/questions/12383371/refresh-a-page-using-php
					header("Location: login.php");
					die;
				default: 
					return [new VistaListaAlumnos($this->_profesor)];
			}
		}
		else{
			return [$this->getMenu(), new VistaMensaje("Acción inválida")];
		}
	}
	
	public function getMenu(){
		return new VistaMenuProfesor();
	}
	
	// Métodos privados
	private function esAccionValida($pAccion){
	  $params = array(
    	'options' => array(
        	'default' => false, // valor a retornar si el filtro falla
        	'min_range' => 1,
			'max_range' => 5)
		);
	  
	  //http://php.net/manual/es/function.filter-var.php
	  return filter_var($pAccion, FILTER_VALIDATE_INT, $params);
  	}
  

	
}

