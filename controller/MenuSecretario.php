<?php 

require_once(__DIR__ ."/../view/VistaMenuSecretario.php");
require_once(__DIR__ ."/../view/VistaMensaje.php");
require_once(__DIR__ ."/../view/VistaMatricularAlumno.php");

class MenuSecretario {
	
	// Atributos
	private $_secretario;
	
	// Getters y Setters
	
	
	// Construccion
	public function __construct(Secretario $pSecretario){
		$this->_secretario = $pSecretario;
	}
	
	// Métodos de interface
	public function procesaAccion(array $request){
		$accion = !isset($request["accion"]) ? 0 : $request["accion"];
		if($accion == 0){
			return [$this->getMenu(), new VistaMensaje("Selecciona una opción")];
		}
		else if($this->esAccionValida($accion)){
			switch($accion){
				case 1:
					$alumno = $this->_secretario->getAlumnoFromDni($request["dni"]);
					return [new VistaMatricularAlumno($alumno, $this->_secretario)];
				case 2:
					$alumno = $this->_secretario->getAlumnoFromDni($request["dni"]);
					foreach ($request as $key => $value) {
						$materia = $this->_secretario->getMateriaPorCodigo($key);
						if($materia != null){
							$alumno->matricularEnMateria($materia);
						}
					}
					return [$this->getMenu(), new VistaMensaje("Alumno Matriculado correctamente")];
				case 3:
					//TODO
					break;
				case 4:
					//TODO:
					break;
				case 5:
					session_destroy();
					// https://stackoverflow.com/questions/12383371/refresh-a-page-using-php
					header("Location: login.php");
					die;
				default: 
					//TODO:
			}
		}
		else{
			//echo("Acción inválida");
			//TODO:
		}
	}
	
	public function getMenu(){
		return new VistaMenuSecretario();
	}
	
	// Métodos privados
	private function esAccionValida($pAccion){
		$params = array(
			'options' => array(
				'default' => false, // valor a retornar si el filtro falla
	        	'min_range' => 1,
				'max_range' => 5)
			);
		
		//http://php.net/manual/es/function.filter-var.php
		return filter_var($pAccion, FILTER_VALIDATE_INT, $params);
	}
}